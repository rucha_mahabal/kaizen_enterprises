<?php include 'header.php'; ?>

<!-- hero section -->
   <section id="hero">
        <div id="typed-para" class="container">
            <div class="row">
                <div class="col">
                </div>
                <div class="col" align="center">
                    <center>
                    <h1>Kaizen Enterprises</h1>
                    <div id="typed-strings">
                        <p>The <strong>"Kaizen Enterprises"</strong> is promoted by three qualified Engineers.</p><p>They have vast cumulative experience of 26 years in the field of Hard facing, Metalizing of various components. </p>
                        <p><em>"Kai"</em> means <em>"Change"</em> and <em>"Zen"</em> means <em>"For the better".</em></p>
                        <p>Change for the better becomes an ongoing process when every person keeps making improvements (no matter how small) continuously. 
                        </p>
                    </div>
                    <span id="typed1"></span>
                    </center>
                </div>
            </div>
        </div>
   </section>

   <!-- services -->
   <section id="services">
       <h1 align="center">Our Services</h1>
   <div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-8">
            <div class="serviceBox green">
                <div class="service-icon">
                    <i class="fas fa-cogs"></i>
                </div>
                <h3 class="title">Eutectic Welding Process</h3>
                <p class="description">
                   A special process to overcome the handicaps of the fields related to per and post heat, heat of application, base metal distortion and dilution, cracking, bond strength, amperage ranges, weldabililty and solidification rates using special filler alloys by tungsten inert gas, a welding, process for critical applications requiring high quality weld.     
                </p>
            </div>
        </div>
        <div class="col-md-4 col-sm-8">
            <div class="serviceBox pink">
                <div class="service-icon">
                   <i class="fas fa-spray-can"></i>
                </div>
                <h3 class="title">Thermal spraying Process</h3>
                <p class="description">
                A wide range of powder alloys which are spread with special delivery systems are available for spraying metallic powders of fusible and non-fusible grades. These can be used for reclaiming worn out parts.
                </p>
            </div>
        </div>
        <div class="col-md-4 col-sm-8">
            <div class="serviceBox green">
                <div class="service-icon">
                   <i class="fas fa-cogs"></i>
                </div>
                <h3 class="title">Process Cold Welding</h3>
                <p class="description">
                A revolutionary process / concept in the field of repairs and protective maintenance. These products are three dimensionally stable compounds, which form a strong bond with virtually all industrial base metals. They offer quick, simple and cost effective repair solutions for a wide spectrum of worn out and damaged components. It offers a huge potential of savings in time, money and resources.
                </p>
            </div>
        </div>
    </div>

</section>

<!-- testimonials section -->
<section id="testimonials">
        <div class="overlay"></div>
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="assets/sparkles_abdulkerim.mp4" type="video/mp4">
        </video>
        <div class="container" id="content">
                    <div class="row">
                        <div class="col-md-8 offset-md-2 col-10 offset-1 mt-5">
                            <h1 class="text-center mt-5 mb-5 pb-2"><strong>Testimonials</strong></h1>
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner mt-4">
                                <div class="carousel-item text-center active">
                                    <div class="img-box p-1 border rounded-circle m-auto">
                                        <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-1.jpg" alt="First slide">
                                    </div>
                                    <h5 class="mt-4 mb-0"><strong class="text-primary text-uppercase">Paul Mitchel</strong></h5>
                                    <h6 class="text-light m-0">Client1</h6>
                                    <p class="m-0 pt-3 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                                </div>
                                <div class="carousel-item text-center">
                                    <div class="img-box p-1 border rounded-circle m-auto">
                                        <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-3.jpg" alt="First slide">
                                    </div>
                                    <h5 class="mt-4 mb-0"><strong class="text-primary text-uppercase">Steve Fonsi</strong></h5>
                                    <h6 class="text-light m-0">Client2</h6>
                                    <p class="m-0 pt-3 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                                </div>
                                <div class="carousel-item text-center">
                                    <div class="img-box p-1 border rounded-circle m-auto">
                                        <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-7.jpg" alt="First slide">
                                    </div>
                                    <h5 class="mt-4 mb-0"><strong class="text-primary text-uppercase">Daniel vebar</strong></h5>
                                    <h6 class="text-light m-0">Client3</h6>
                                    <p class="m-0 pt-3 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            </div>
                        </div>            
                    </div>
</section>

<!-- our clients -->
<section id="clients">
<h1 class="text-center mt-5 mb-5 pb-2"><strong>Our Clients</strong></h1>
      <div class="row">
         <div class="col">
              <img src="assets\images\clients\Akay.png">
         </div>
         <div class="col">
              <img src="assets\images\clients\Alfa_Laval.svg.png">
         </div>
         <div class="col">
              <img src="assets\images\clients\dembla-client.jpg">
         </div>
         <div class="col">
              <img src="assets\images\clients\image_prab-red-logo.jpg">
         </div>
         <div class="col">
              <img src="assets\images\clients\Kirloskar_Group_Logo.png">
         </div>
      </div>
      <div class="row">
         <div class="col">
              <img src="assets\images\clients\Ksblogo.png">
         </div>
         <div class="col">
              <img src="assets\images\clients\Niton Valve Industries Pvt Ltd.jpg">
         </div>
         <div class="col">
              <img src="assets\images\clients\RK.jpg">
         </div>
         <div class="col">
              <img src="assets\images\clients\sulzer.jpg">
         </div>
         <div class="col">
              <img src="assets\images\clients\super.png">
         </div>
      </div>
</section>
<?php include 'footer.php'; ?>