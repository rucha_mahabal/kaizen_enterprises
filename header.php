<!doctype html>
<html lang="en">
  <head>
    <title>Kaizen Enterprises</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- custom css -->
    <link rel="stylesheet" href="css/custom.css">
    
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

		<!-- transparent to solid navbar script -->
		<script type="text/javascript">
		     $(window).on('scroll', function(){
					 if($(window).scrollTop()){
						 $('nav').addClass('solid');
					 }
					 else{
						 $('nav').removeClass('solid');
					 }
				 })
		</script>
		<script src="js/typed.js"></script>
    
  </head>
  <body>

	<!-- =========navbar========= -->
	<header class="site-header" role="banner">
      <nav id="navbar-main" class="navbar navbar-expand-lg navbar-dark fixed-top">
        <!-- <img id="navbar-logo" src="assets/images/logo.jpg" width="180" height="60" alt=""> -->
        <!-- <h3><a class="navbar-brand" href="#hero-section"></a></h3> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Technical</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Our Clients</a>
			</li>
			<li class="nav-item">
              <a class="nav-link" href="#">Certifications</a>
			</li>
			<li class="nav-item">
              <a class="nav-link" href="#">Contact Us</a>
            </li>
          </ul>
        </div>
       </nav>
    </header>
